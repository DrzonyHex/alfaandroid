package com.example.drzony.myapplication;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.util.Date;

public class SensorService extends Service implements SensorEventListener {

    private static String fileName = "blah";


    public static class MyReceiver extends BroadcastReceiver {
        public MyReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            // This method is called when this BroadcastReceiver receives an Intent broadcast.
            Toast.makeText(context, "Action: " + intent.getAction(), Toast.LENGTH_SHORT).show();
           // Bundle extras = ((Activity) context).getIntent().getExtras();
            Bundle extras = intent.getExtras();
            if (extras != null) {
                fileName = extras.getString("fileName");
            }
        }
    }

    private float[] gravity = new float[3];
    private float[] linear_acceleration = new float[3];

    private SensorManager mSensorManager;
   // private Sensor mSensorLight;
    private Sensor mSensorAccelerometer;

    private String filePath = "SensorData";
    File myExternalAccFile;

    private boolean record;
    private EditText accT;
    private float light_value;

    @Override
    public void onCreate() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //eeeee ?
        assert mSensorManager != null;
        //mSensorLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        String sensor_error = getResources().getString(R.string.error_no_sensor);

        record = false;




        //checking if sensors are available
        if (mSensorAccelerometer == null) {
            Toast.makeText(this, sensor_error, Toast.LENGTH_LONG).show();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        mSensorManager.registerListener(this, mSensorAccelerometer,
                SensorManager.SENSOR_DELAY_NORMAL);
        Toast.makeText(this, "Service Started (onStartCommand)", Toast.LENGTH_LONG).show();
        record=true;

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSensorManager.unregisterListener(this);
        record=false;
        //adding date of taking measurment in the end of file.
        saveAccData(" " + new Date() + " ");
        Toast.makeText(this, "Service Destroyed (onDestroy)", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //Use the sensor property of the SensorEvent to get a Sensor object, and then use getType() to get the type of that sensor.
        // Sensor types are defined as constants in the Sensor class, for example, Sensor.TYPE_LIGHT.
        int sensorType = event.sensor.getType();
        // float currentValueX = event.values[0];

        float currentValueX = event.values[0];
        float currentValueY = event.values[1];
        float currentValueZ = event.values[2];

        switch (sensorType) {
            case Sensor.TYPE_ACCELEROMETER:

                final float alpha = 0.8f;

                gravity[0] = alpha * gravity[0] + (1 - alpha) * currentValueX;
                gravity[1] = alpha * gravity[1] + (1 - alpha) * currentValueY;
                gravity[2] = alpha * gravity[2] + (1 - alpha) * currentValueZ;

                linear_acceleration[0] = currentValueX - gravity[0];
                linear_acceleration[1] = currentValueY - gravity[1];
                linear_acceleration[2] = currentValueZ - gravity[2];


                    // Event came from the light sensor.
                    //case Sensor.TYPE_LIGHT:
                        // Handle light sensor
                        // mTextSensorLight.setText(getResources().getString( R.string.label_light, currentValueX)); //label_light from the resources
              /*  if(currentValueX>2000){
                    Toast.makeText(this, "Light Sensor Value ABOVE 500 :) ", Toast.LENGTH_LONG).show();
                   // mTextInfo.setText(getResources().getString( R.string.label_info, "Light Sensor Value ABOVE 500 :) "));
                }
                else if(currentValueX < 5){
                    Toast.makeText(this, "Light Sensor Value UNDER 500 :( ", Toast.LENGTH_LONG).show();
                    //mTextInfo.setText(getResources().getString( R.string.label_info, "Light Sensor Value UNDER 500 :( "));
                }
                */
                if (record) {
                  //saveAccData(currentValueX + ";" + new Date());
                  saveAccData(linear_acceleration[0] + " ; " + linear_acceleration[1] + " ; " + linear_acceleration[2]);
                  }
                break;

              default:
                        // do nothing
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }




    private void saveAccData(String data){
        try {
            myExternalAccFile = new File(getExternalFilesDir(filePath), fileName + ".txt");
            FileWriter fw = new FileWriter(myExternalAccFile,true);

            fw.append(data + "\r\n");

            fw.close();
        } catch (Exception e) {
            Log.e("EXTERNAL_STORAGE", e.getMessage(), e);
        }
    }




}




