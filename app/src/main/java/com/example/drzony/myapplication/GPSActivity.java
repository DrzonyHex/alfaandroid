package com.example.drzony.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class GPSActivity extends AppCompatActivity {

    static int PERIOD_IS_LOCATION_SIGNIFICANTLY_NEWER = 60 * 1000;
    static int PERIOD_GET_NEW_LOCATION = 15 * 1000;
    static int MIN_DISTANCE = 10; //meters
    private static final int PERMISSION_REQUEST = 0;
    LocationManager locationManager;
    double longitudeBest, latitudeBest;
    double longitudeGPS, latitudeGPS;
    double accuracyGPS, accuracyNetwork;
    String timeGPS, timeNetwork;
    String longitudeGPSstring, latitudeGPSstring;
    String longitudeNetworkstring, latitudeNetworkstring;
    String accuracyGPSstring, accuracyNetworkstring;
    double longitudeNetwork, latitudeNetwork;
    TextView longitudeValueBest, latitudeValueBest;
    TextView labelLocalizationGPS, labelLocalizationNetwork, labelLocalizationBest;
    TextView longitudeValueNetwork, latitudeValueNetwork;
    Location currentBestLocation;
    Location currentGPSLocation;
    Location currentNetworkLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        labelLocalizationGPS = (TextView) findViewById(R.id.label_localization_gps);
        labelLocalizationNetwork = (TextView) findViewById(R.id.label_localization_network);
        labelLocalizationBest = findViewById(R.id.label_localization_best);
    }

    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    public void toggleGPSUpdates(View view) {
        if (!checkLocation())
            return;
        Button button = (Button) view;
        if (button.getText().equals(getResources().getString(R.string.pause))) {
            locationManager.removeUpdates(locationListenerGPS);
            button.setText(R.string.resume);
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                ActivityCompat.requestPermissions(GPSActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST);
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, PERIOD_GET_NEW_LOCATION, MIN_DISTANCE, locationListenerGPS);
            button.setText(R.string.pause);
        }
    }


    private final LocationListener locationListenerGPS = new LocationListener() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
        public void onLocationChanged(final Location location) {

            currentGPSLocation = location;
            longitudeGPS = location.getLongitude();
            latitudeGPS = location.getLatitude();
            accuracyGPS = location.getAccuracy();

            longitudeGPSstring = String.valueOf(longitudeGPS);
            latitudeGPSstring = String.valueOf(latitudeGPS);
            accuracyGPSstring = String.valueOf(accuracyGPS);
            timeGPS = String.valueOf(location.getElapsedRealtimeNanos());


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //longitudeValueGPS.setText(longitudeGPS + "");
                    // if(longitudeGPS != 0

                    labelLocalizationGPS.setText(getResources().getString(R.string.label_localization_gps, longitudeGPSstring, latitudeGPSstring, accuracyGPSstring, timeGPS));//, latitudeGPS));

                    // latitudeValueGPS.setText(latitudeGPS + "");
                    Toast.makeText(GPSActivity.this, "GPS Provider update", Toast.LENGTH_SHORT).show();
                    toggleBestUpdates(currentBestLocation, location);

                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };


    public void toggleNetworkUpdates(View view) {
        if (!checkLocation())
            return;
        Button button = (Button) view;
        if (button.getText().equals(getResources().getString(R.string.pause))) {
            locationManager.removeUpdates(locationListenerNetwork);
            button.setText(R.string.resume);
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                ActivityCompat.requestPermissions(GPSActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST);
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, PERIOD_GET_NEW_LOCATION, MIN_DISTANCE, locationListenerNetwork);
            Toast.makeText(this, "Network provider started running", Toast.LENGTH_LONG).show();
            button.setText(R.string.pause);
        }
    }


    private final LocationListener locationListenerNetwork = new LocationListener() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
        public void onLocationChanged(final Location location) {

            currentNetworkLocation = location;
            longitudeNetwork = location.getLongitude();
            latitudeNetwork = location.getLatitude();
            accuracyNetwork = location.getAccuracy();

            longitudeNetworkstring = String.valueOf(longitudeNetwork);
            latitudeNetworkstring = String.valueOf(latitudeNetwork);
            accuracyNetworkstring = String.valueOf(accuracyNetwork);
            timeNetwork = String.valueOf(location.getElapsedRealtimeNanos());

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //longitudeValueGPS.setText(longitudeGPS + "");
                    // if(longitudeGPS != 0

                    labelLocalizationNetwork.setText(getResources().getString(R.string.label_localization_network, longitudeNetworkstring, latitudeNetworkstring, accuracyNetworkstring, timeNetwork));//, latitudeGPS));

                    // latitudeValueGPS.setText(latitudeGPS + "");
                    Toast.makeText(GPSActivity.this, "Network Provider update", Toast.LENGTH_SHORT).show();
                    toggleBestUpdates(currentBestLocation, location);
                }
            });
        }


        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };


    public void requestInstantLocation(View view) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
               ActivityCompat.requestPermissions(GPSActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_REQUEST);
            return;
        }
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 0, 0, locationListenerGPS);
        Toast.makeText(this, "Maximum location requesting!", Toast.LENGTH_LONG).show();


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null && location != null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getElapsedRealtimeNanos() - currentBestLocation.getElapsedRealtimeNanos();
        boolean isSignificantlyNewer = timeDelta > PERIOD_IS_LOCATION_SIGNIFICANTLY_NEWER ;
        boolean isSignificantlyOlder = timeDelta < PERIOD_IS_LOCATION_SIGNIFICANTLY_NEWER ;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 100;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void toggleBestUpdates(Location currentBestLocation_, Location currentReceivedLocation_) {
        if(isBetterLocation(currentReceivedLocation_, currentBestLocation_) && currentReceivedLocation_ != null) {
            currentBestLocation = currentReceivedLocation_;
            labelLocalizationBest.setText(String.valueOf(currentBestLocation.getProvider()));
        }
    }







   /* @Override
    public void onLocationChanged(Location loc) {

        String longitude = "Longitude: " + loc.getLongitude();
        mTextGPSLocalization.setText(getResources().getString( R.string.label_localization_gps2, longitude));
        String latitude = "Latitude: " + loc.getLatitude();



    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}


    public void getGPSLocation(View view) {
        //mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //List<Sensor> deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);

    }*/


}
