package com.example.drzony.myapplication;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ForDetectionService extends AppCompatActivity {

    private static EditText pNumber;
    TextView labelPeak;

    public static String getpNumber() {
        return pNumber.getText().toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_detection_service);
        pNumber = (EditText) findViewById(R.id.pNumber);


    }

    public void startService(View view) {
        startService(new Intent(getBaseContext(), DetectionService.class));

        Toast.makeText(this, "Service Started (startService)", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();
        //intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        intent.setAction("com.example.drzony.myapplication.BroadcastReceiverDetection");
        if(pNumber.getText().toString()!=null && pNumber.getText().toString().length()>=9) {
            intent.putExtra("phoneNumber", pNumber.getText().toString());
        }
        else {
            Toast.makeText(this, "Probably typo in the number. Check it !", Toast.LENGTH_SHORT).show();
            intent.putExtra("phoneNumber", "000000000");
        }
        sendBroadcast(intent);
    }

    // Method to stop the service
    public void stopService(View view) {
        stopService(new Intent(getBaseContext(), DetectionService.class));
        Toast.makeText(this, "Service Stopped (stopService)", Toast.LENGTH_SHORT).show();
    }

}
