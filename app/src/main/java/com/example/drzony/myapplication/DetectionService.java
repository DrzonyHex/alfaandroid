package com.example.drzony.myapplication;


import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;

import java.io.File;
import java.util.Date;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Timer;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class DetectionService extends Service implements SensorEventListener {

    int countOfMeasurements;
    final float FALL_LIMIT = 7; //10 // wartosc powyzej ktorej sadzimy ze byl upadek
    final float SILENCE_LIMIT = 2; // wartosc poniżej ktorej sadzimy nie ruszamy sie po wypadku
    final int SIZE = 100;  //chemy pamietac 10sekund i miec 10pomiarów z kazdej sekundy
    static LinkedList<Float> dataStream;

    public static String CHANNEL_ID = "DETECTION";
    private static String pNumber;
    private int theme;
    static int remainingTimeToSendSmS=10;
    static CountDownTimer timerSMS;
    int remainingTime = 10000;
    int countDownTime = 1000;

    static Boolean sendSms = false;
    static Boolean isNotification = false;
    static Boolean destroyNotification = false;


    public static void restoreRemainingTimeToSendSms(){
        remainingTimeToSendSmS=10;
    }
    public void decrementRemainingTimeToSendSms(){
        remainingTimeToSendSmS=remainingTimeToSendSmS-1;
    }

    public static void setIsNotification(Boolean value) {
        isNotification = value;
    }
    public static Boolean getIsNotification() {
        return isNotification;
    }

    public static void setSendSms(Boolean value) {
        sendSms = value;
    }
    public static Boolean getSendSms() {
        return sendSms;
    }

    public static void setDestroyNotification(Boolean value) {
        destroyNotification = value;
    }
    public static Boolean getDestroyNotification() {
        return destroyNotification;
    }


    public static class MyReceiverDetection extends BroadcastReceiver {
        public MyReceiverDetection() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            // This method is called when this BroadcastReceiver receives an Intent broadcast.
            Toast.makeText(context, "Action: " + intent.getAction(), Toast.LENGTH_SHORT).show();
            // Bundle extras = ((Activity) context).getIntent().getExtras();
            Bundle extras = intent.getExtras();
            if (extras != null) {
                pNumber = extras.getString("phoneNumber");
            }
        }
    }

    public static class MyReceiverNotification extends BroadcastReceiver {
        public MyReceiverNotification() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {


            if(intent.getAction()=="cancelTimerBroadcast") {
                // This method is called when this BroadcastReceiver receives an Intent broadcast.
                Toast.makeText(context, "Action: " + intent.getAction(), Toast.LENGTH_SHORT).show();
                // Bundle extras = ((Activity) context).getIntent().getExtras();
                //com.example.drzony.myapplication.DetectionService.cancelTimerSMS();
                com.example.drzony.myapplication.DetectionService.setSendSms(false);
                Toast.makeText(context, "SendSMS? : " + com.example.drzony.myapplication.DetectionService.getSendSms(), Toast.LENGTH_SHORT).show();
                com.example.drzony.myapplication.DetectionService.restoreRemainingTimeToSendSms();
                DetectionService.setDestroyNotification(true);
                DetectionService.notificationManager.cancel(R.string.NOTIFICATION_DETECTION_ID);
                DetectionService.setIsNotification(false);

            }

            else if(intent.getAction()=="cancelTimerBroadcastAndSendSms") {

                DetectionService.setDestroyNotification(true);
                DetectionService.notificationManager.cancel(R.string.NOTIFICATION_DETECTION_ID);
                DetectionService.setIsNotification(false);
                com.example.drzony.myapplication.DetectionService.restoreRemainingTimeToSendSms();
                //Toast.makeText(context, "HERE SEND SMS", Toast.LENGTH_SHORT).show();
                DetectionService.sendSMS(pNumber);
            }
        }
    }

    private float[] gravity = new float[3];
    private float[] linear_acceleration = new float[3];

    private SensorManager mSensorManager;
    // private Sensor mSensorLight;
    private Sensor mSensorAccelerometer;
    public static NotificationManagerCompat notificationManager;

    private boolean record;
    private float light_value;
    private boolean detect;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.

        mSensorManager.registerListener(this, mSensorAccelerometer,
                SensorManager.SENSOR_DELAY_NORMAL); // 5 pomiarow / sek
        Toast.makeText(this, "onStart Command Detection(onStartCommand)", Toast.LENGTH_SHORT).show();
        record=true;
        detect=true;

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSensorManager.unregisterListener(this);
        detect=false;
        Toast.makeText(this, "ServiceDetection Destroyed  (onDestroy)", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCreate() {

        countOfMeasurements=0;
        notificationManager = NotificationManagerCompat.from(this);
        createNotificationChannel(); //for notification API >= 26

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //eeeee ?
        assert mSensorManager != null;
        //mSensorLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        String sensor_error = getResources().getString(R.string.error_no_sensor);

        record = false;
        detect = false;

        dataStream = new LinkedList<Float>(); //lista do przechowywaania chwilowych odczytow potrzebnych do algorytmu wykrywania upadku

        //checking if sensors are available
        if (mSensorAccelerometer == null) {
            Toast.makeText(this, sensor_error, Toast.LENGTH_LONG).show();
        }




    }



    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onSensorChanged(SensorEvent event) {
        //Use the sensor property of the SensorEvent to get a Sensor object, and then use getType() to get the type of that sensor.
        // Sensor types are defined as constants in the Sensor class, for example, Sensor.TYPE_LIGHT.
        int sensorType = event.sensor.getType();
        float currentValueX = event.values[0];
        float currentValueY = event.values[1];
        float currentValueZ = event.values[2];



        switch (sensorType) {
            case Sensor.TYPE_ACCELEROMETER:

                final float alpha = 0.8f;

                gravity[0] = alpha * gravity[0] + (1 - alpha) * currentValueX;
                gravity[1] = alpha * gravity[1] + (1 - alpha) * currentValueY;
                gravity[2] = alpha * gravity[2] + (1 - alpha) * currentValueZ;

                linear_acceleration[0] = currentValueX - gravity[0];
                linear_acceleration[1] = currentValueY - gravity[1];
                linear_acceleration[2] = currentValueZ - gravity[2];

                addToDataStream(countPeak(linear_acceleration[0],linear_acceleration[1],linear_acceleration[2]));

                if (detect) {
                    if(countOfMeasurements==99) {
                        String peakValueString = String.valueOf(countPeak(linear_acceleration[0],linear_acceleration[1],linear_acceleration[2]));
                        Toast.makeText(this, peakValueString, Toast.LENGTH_SHORT).show();
                        //if fallCheck(linear_acceleration[0], linear_acceleration[1], linear_acceleration[2]){
                        if (fallCheck2()) {
                            // showFallAlertDialog();
                            Toast.makeText(this, "ONCHANGED Detection(onStartCommand)", Toast.LENGTH_SHORT).show();
                            if (isNotification == false && destroyNotification == true)
                                setDestroyNotification(false);
                            createNotification();
                            //createtimerSMS(remainingTime,countDownTime);
                        }
                        countOfMeasurements=0;
                    }
                    else
                        countOfMeasurements++;
                }
                break;

            default:
                // do nothing
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /////// DO ALGORYTMU WYKRYWANIA UPADKU /////////////////

    public float countPeak(float xAxis, float yAxis, float zAxis) {
        float peak = (float) sqrt(pow(xAxis,2) + pow(yAxis,2) + pow(zAxis,2));// aktualna wartosc przyspieszenia
       // System.out.println("aktualna dlugosc wektora " + peak);
        return peak;
    }

    public void addToDataStream(float actualPeak) { //dodajemy aktualna wartosc do naszej kolejki
        if(dataStream.size() <= SIZE) {
            dataStream.add(actualPeak);
        }
        if(dataStream.size() > SIZE) {
            dataStream.remove();
            dataStream.add(actualPeak);
        }
    }
    // funkcje do wykrywania upadku
    private boolean fallCheck(float xVal, float yVal, float zVal){
        if (xVal > 10) {
            return true;
        }
        else return false;
    }

    public boolean fallCheck1() {
        for(int i = 0 ; i <= dataStream.size() - 5; i++) {
            if ( dataStream.get(i) >= FALL_LIMIT && dataStream.get(i + 1) <= SILENCE_LIMIT
                    && dataStream.get(i + 2) <= SILENCE_LIMIT
                    && dataStream.get(i + 3) <= SILENCE_LIMIT
                    && dataStream.get(i + 4) <= SILENCE_LIMIT
                    && dataStream.get(i + 5) <= SILENCE_LIMIT
                    && dataStream.get(i + 6) <= SILENCE_LIMIT
                    && dataStream.get(i + 7) <= SILENCE_LIMIT
                    && dataStream.get(i + 8) <= SILENCE_LIMIT
                    && dataStream.get(i + 9) <= SILENCE_LIMIT
                    ) {
                return true;
            }
        }
        return false;
    }

    public boolean fallCheck2(){
        for(int i = 0 ; i < dataStream.size() - 5; i++) {
            if ( dataStream.get(i) >= FALL_LIMIT ) {
                int valuesToEnd = dataStream.size() - i;
                float sumOtherElements = 0;
                for ( int j = i ; j < dataStream.size(); j++) {
                    sumOtherElements += dataStream.get(j);
                }
                float average = sumOtherElements / valuesToEnd ;
                if ( average <= 2 ) {
                    return true;
                }
            }
        }
        return false;
    }


    public void createtimerSMS(int remainingTime, int countDownInterval){
        //5000, 1000
        timerSMS = new CountDownTimer(remainingTime,countDownInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
                //remainingTimeToSendSmS=(int) millisUntilFinished/1000;
                if(millisUntilFinished<5000) {
                    timerSMS.cancel();
                    //Toast.makeText(DetectionService.this, "CANCELLLED", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFinish() {
                 sendSMS(pNumber);
                Toast.makeText(DetectionService.this, "ON FINISHOOOO " , Toast.LENGTH_SHORT).show();
            }
        }.start();
    }

    public static void cancelTimerSMS() {
        timerSMS.cancel();
    }






       // /*final*/ AlertDialog dialog = builder.create();
       // dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);//WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG);
       // dialog.show();


    public static void sendSMS(String phoneNumber){

        String SMS_MESSAGE = "Właśnie miałem upadek! Potrzebuję pomocy!";
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, SMS_MESSAGE, null,null);
    }


    private void createNotification(){
       /* if(Build.VERSION.SDK_INT >= 8.0) {

        }
        else
        {
            //here version without CHANNEL_ID
        }*/
       // INTENTS:
        setIsNotification(true);
        Intent intent = new Intent(this, ForDetectionQuestion.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Intent yesIntent = new Intent(this, MyReceiverNotification.class);
        yesIntent.setAction("cancelTimerBroadcast");
        PendingIntent yesPendingIntent =
                PendingIntent.getBroadcast(this, 0, yesIntent, 0);

        Intent helpIntent = new Intent(this, MyReceiverNotification.class);
        helpIntent.setAction("cancelTimerBroadcastAndSendSms");
        PendingIntent helpPendingIntent =
                PendingIntent.getBroadcast(this, 0, helpIntent, 0);





        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_warning_black_24dp)
                .setContentTitle("Fall detected !")
                .setContentText("Are you ok ?")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setCategory(NotificationCompat.CATEGORY_ALARM) //Works even in DONT DISTURB mode
                .setContentIntent(pendingIntent)// Set the intent that will fire when the user taps the notification
                .addAction(R.drawable.ic_warning_black_24dp, getString(R.string.help),helpPendingIntent)
                .addAction(R.drawable.ic_warning_black_24dp, getString(R.string.yes),yesPendingIntent)
                .setAutoCancel(true)//automatically removes the notification when the user taps it.
                .setLights(Color.RED, 500, 500)
                .setVibrate(new long[] {1000, 1000, 1000, 1000, 1000, 1000})
                .setSound(alarmSound)
                .setOnlyAlertOnce(true);


        final int PROGRESS_MAX = 10;
        final int PROGRESS_CURRENT = 0;//10 - remainingTimeToSendSmS;
        final int PROGRESS_COUNTDOWN = 0;
        mBuilder.setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(R.string.NOTIFICATION_DETECTION_ID, mBuilder.build());

        CountDownTimer timer = new CountDownTimer(10000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if(getDestroyNotification()==false) {
                    int PROGRESS_CURRENT = (int) (10 - millisUntilFinished / 1000);//10 - remainingTimeToSendSmS;
                    mBuilder.setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);
                    // notificationId is a unique int for each notification that you must define
                    notificationManager.notify(R.string.NOTIFICATION_DETECTION_ID, mBuilder.build());
                    //decrementRemainingTimeToSendSms();
                }
                else{
                    cancel();
                   // setDestroyNotification(false);
                }

            }

            @Override
            public void onFinish() {
                // sendSMS(pNumber);
                mBuilder.setContentText("Message for help has been sent")
                       .setProgress(0,0,false);
                notificationManager.notify(R.string.NOTIFICATION_DETECTION_ID, mBuilder.build());
               // Toast.makeText(DetectionService.this, "FINISHOOO", Toast.LENGTH_SHORT).show();
               // restoreRemainingTimeToSendSms();
                sendSMS(pNumber);
            }
        }.start();


    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.CHANNEL_ID_DETECTION);
            String description = getString(R.string.CHANNEL_ID_DETECTION);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(getString(R.string.CHANNEL_ID_DETECTION), name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


}
