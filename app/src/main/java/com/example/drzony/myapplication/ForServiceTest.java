package com.example.drzony.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ForServiceTest extends AppCompatActivity {
String msg = "Androiddd";

    private EditText accT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_service_test);
        Log.d(msg, "The onCreate() event");

        accT = (EditText) findViewById(R.id.accEditText);

    }

    public void startService(View view) {
        startService(new Intent(getBaseContext(), SensorService.class));

            Toast.makeText(this, "Service Started (startService)", Toast.LENGTH_SHORT).show();



            Intent intent = new Intent();
            //intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            intent.setAction("com.example.drzony.myapplication.BroadcastReceiver");
            if(accT.getText().toString()!=null) {
                intent.putExtra("fileName", accT.getText().toString());
            }
            else {
                intent.putExtra("fileName", "default_name");
            }
            sendBroadcast(intent);

    }

    // Method to stop the service
    public void stopService(View view) {
        stopService(new Intent(getBaseContext(), SensorService.class));
        Toast.makeText(this, "Service Stopped (stopService)", Toast.LENGTH_SHORT).show();
    }
}