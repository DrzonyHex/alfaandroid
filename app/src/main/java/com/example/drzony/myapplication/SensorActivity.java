package com.example.drzony.myapplication;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class SensorActivity extends AppCompatActivity implements SensorEventListener{

    private SensorManager mSensorManager;
    // Individual light and proximity sensors.
    private Sensor mSensorProximity;
    private Sensor mSensorLight;
    private Sensor mSensorAccelerometer;
    private Sensor mSensorGyroscope;
    // TextViews to display current sensor values
    private TextView mTextSensorLight;
    private TextView mTextSensorProximity;
    private TextView mTextSensorAccelerometer;
    private TextView mTextSensorGyroscope;
    private TextView mTextInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sensor);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        mTextSensorLight = (TextView) findViewById(R.id.label_light);
        mTextSensorProximity = (TextView) findViewById(R.id.label_proximity);
        mTextSensorAccelerometer = (TextView) findViewById(R.id.label_accelerometer);
        mTextSensorGyroscope = (TextView) findViewById(R.id.label_gyroscope);
        mTextInfo = (TextView) findViewById(R.id.label_info);

        mSensorProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mSensorLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        String sensor_error = getResources().getString(R.string.error_no_sensor);

        //checking if sensors are available
        if (mSensorLight == null) {
            mTextSensorLight.setText(sensor_error);
        }
        if (mSensorProximity == null) {
            mTextSensorProximity.setText(sensor_error);
        }
        if (mSensorAccelerometer == null) {
            mTextSensorProximity.setText(sensor_error);
        }
        if (mSensorGyroscope == null) {
            mTextSensorProximity.setText(sensor_error);
        }
/*
        if(mLight == null)
        {
            Toast.makeText(this, "The device has no light sensor", Toast.LENGTH_SHORT).show();
        }
        if(mAccelerometer == null)
        {
            Toast.makeText(this, "The device has no accelerometer sensor", Toast.LENGTH_SHORT).show();
        }
        if(mGyroscope == null)
        {
            Toast.makeText(this, "The device has no gyroscope sensor", Toast.LENGTH_SHORT).show();
        }
*/
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        //Use the sensor property of the SensorEvent to get a Sensor object, and then use getType() to get the type of that sensor.
        // Sensor types are defined as constants in the Sensor class, for example, Sensor.TYPE_LIGHT.
        int sensorType = event.sensor.getType();
            float currentValueX = event.values[0];
            float currentValueY = event.values[1];
            float currentValueZ = event.values[2];

            switch (sensorType) {
                // Event came from the light sensor.
                case Sensor.TYPE_LIGHT:
                    // Handle light sensor
                    mTextSensorLight.setText(getResources().getString( R.string.label_light, currentValueX)); //label_light from the resources
                    if(currentValueX>500){
                        mTextInfo.setText(getResources().getString( R.string.label_info, "Light Sensor Value ABOVE 500 :) "));
                    }
                    else{
                        mTextInfo.setText(getResources().getString( R.string.label_info, "Light Sensor Value UNDER 500 :( "));
                    }

                    break;
                case Sensor.TYPE_PROXIMITY:
                    mTextSensorProximity.setText(getResources().getString( R.string.label_proximity, currentValueX));
                    break;
                case Sensor.TYPE_ACCELEROMETER:
                    mTextSensorAccelerometer.setText(getResources().getString( R.string.label_accelerometer, currentValueX, currentValueY, currentValueZ));
                    break;
                case Sensor.TYPE_GYROSCOPE:
                    mTextSensorGyroscope.setText(getResources().getString( R.string.label_gyroscope, currentValueX, currentValueY, currentValueZ));
                    break;
                default:
                    // do nothing
            }
        }

        @Override
        protected void onStart() {
            super.onStart();

            if (mSensorProximity != null) {
            mSensorManager.registerListener(this, mSensorProximity,
                    SensorManager.SENSOR_DELAY_NORMAL);
            }
            if (mSensorLight != null) {
                mSensorManager.registerListener(this, mSensorLight,
                        SensorManager.SENSOR_DELAY_NORMAL);
            }
            if (mSensorAccelerometer != null) {
                mSensorManager.registerListener(this, mSensorAccelerometer,
                        SensorManager.SENSOR_DELAY_NORMAL);//FASTEST
            }
            if (mSensorGyroscope != null) {
                mSensorManager.registerListener(this, mSensorGyroscope,
                        SensorManager.SENSOR_DELAY_NORMAL);//FASTEST
            }
    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregister all the registered listeners
        //method prevents the device from using power when the app is not visible.
        mSensorManager.unregisterListener(this);
    }
/*
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }
*/


}
